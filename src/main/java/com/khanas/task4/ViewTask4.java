package com.khanas.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ViewTask4 {

    private Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);

    public ViewTask4() {

        List<String> list = new ArrayList<>();
        logger.info("Enter strings");
        String strList = sc.nextLine();

        while (!strList.equals("")) {
            list.add(strList);
            strList = sc.nextLine();
        }

        List<String> listOfWords = list.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .collect(Collectors.toList());
        logger.info("List with words: " + listOfWords);

        long countUnique = listOfWords.stream()
                .distinct()
                .count();
        logger.info("Count of unique words: " + countUnique);

        List<String> sorted = listOfWords.stream()
                .sorted()
                .collect(Collectors.toList());
        logger.info("Sorted: " + sorted);

        for (String string : listOfWords.stream().distinct()
                .collect(Collectors.toList())) {
            long countOfWords = list.stream()
                    .filter(s -> s.equals(string))
                    .count();
            logger.info(string + " - " + countOfWords);
        }

        long symbols = listOfWords.stream()
                .map(x->x.split(""))
                .flatMap(Arrays::stream)
                .filter(s->!s.equals(s.toUpperCase()))
                .count();

        logger.info("Count of symbols: " + symbols);
    }
}
