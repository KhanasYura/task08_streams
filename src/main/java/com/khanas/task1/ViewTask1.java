package com.khanas.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.Integer.max;

public class ViewTask1 {

    private Logger logger = LogManager.getLogger();

    public ViewTask1() {

        FunctionalInterface average = (a, b, c) -> (a + b + c) / 3;
        FunctionalInterface max = (a, b, c) -> max(max(a, b), max(c, b));

        logger.info("Average: " + average.func1(1, 2, 3));
        logger.info("Max: " + max.func1(1, 2, 3));
    }
}
