package com.khanas;

import com.khanas.task1.ViewTask1;
import com.khanas.task2.ViewTask2;
import com.khanas.task3.ViewTask3;
import com.khanas.task4.ViewTask4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Application {
    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);

    public static void main(final String[] args) {

        while (true) {

            logger.info("1.Task1");
            logger.info("2.Task2");
            logger.info("3.Task3");
            logger.info("4.Task4");
            logger.info("5.Exit");
            logger.info("Enter");
            int index = sc.nextInt();
            if (index == 1) {

                new ViewTask1();
            } else if (index == 2) {

                new ViewTask2();
            } else if (index == 3) {

                new ViewTask3();
            } else if (index == 4) {

                new ViewTask4();
            } else if (index == 5) {

                break;
            } else {

                logger.error("Wrong input");
            }
        }
    }
}
