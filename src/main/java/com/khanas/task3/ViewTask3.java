package com.khanas.task3;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ViewTask3 {

    private Logger logger = LogManager.getLogger();

    public ViewTask3() {
        int numberOfIntegers = 10;
        Random random = new Random();
        List<Integer> arr = Stream
                .generate(() -> random.nextInt(numberOfIntegers))
                .limit(numberOfIntegers)
                .collect(Collectors.toList());

        IntSummaryStatistics stats = arr.stream()
                .mapToInt((x) -> x)
                .summaryStatistics();
        logger.info(stats);

        int streamSum = arr.stream()
                .mapToInt(i -> i)
                .sum();
        logger.info("Sum found by stream " + streamSum);

        arr.stream()
                .reduce((a, b) -> a + b)
                .ifPresent(sum -> logger.info("Stream sum: " + sum));

        OptionalDouble average = arr.stream()
                .mapToDouble(i -> i)
                .average();

        long count = arr.stream()
                .mapToDouble(i -> i)
                .filter(v -> v > average.getAsDouble())
                .count();
        logger.info("Number of values that are bigger than average: " + count);
    }
}
