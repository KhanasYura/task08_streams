package com.khanas.task2;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewTask2 {

    private Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);

    public ViewTask2() {

        Map<String, Command> map = new LinkedHashMap<>();

        map.put("anonymous", new Command() {
            @Override
            public void execute(final String value) {
                System.out.println("Anonymous " + value);
            }
        });

        map.put("lambda", value -> System.out.println("Lambda: " + value));
        map.put("method", ViewTask2::print);
        map.put("object", new Human()::print);

        System.out.println("Enter function(anonymous, lambda, method, object)");
        String str = sc.nextLine();
        logger.info("Enter string:");
        String value = sc.nextLine();

        try {
            map.get(str).execute(value);
        } catch (NullPointerException e) {
            logger.error("Wrong input");
        }

    }

    public static void print(final String value) {
        System.out.println("Method " + value);
    }
}
