package com.khanas.task2;

public interface Command {
    void execute(String value);
}
